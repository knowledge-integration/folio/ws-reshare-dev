# Ws Reshare Dev

Yarn Workspace for reshare dev.
Lock file included for repeatability and predictability.

## Getting started

Pull this repository and all the sub modules. 

```
git clone --recurse-submodules git@gitlab.com:knowledge-integration/folio/ws-reshare-dev.git
cd ws-reshare-dev/platform-rs
yarn
yarn stripes serve ./stripes.config.js --okapi=<okapi localtion>

```
